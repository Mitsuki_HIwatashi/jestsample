export interface PrefectureCheck {
  prefCode: number;
  prefName: string;
  checked: boolean; // for checkbox
  disabled: boolean; // for checkbox
}
