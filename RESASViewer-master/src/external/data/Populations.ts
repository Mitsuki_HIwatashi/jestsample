import { Population } from "./Population";

export interface Populations {
  label: string;
  data: Population[];
}
