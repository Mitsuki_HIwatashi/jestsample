import React from "react";
import { Container, Grid, Typography } from "@material-ui/core";
import RestClient from "../external/RestClient";
import { PrefectureCheck } from "./data/PrefectureCheck";
import { PopulationForChart } from "./data/PopulationForChart";
import Converter from "../util/domain/Converter";
import { PrefectureCheckBoxes } from "./PrefectureCheckBoxes";
import { Chart } from "./Chart";

interface TopState {
  prefectures: PrefectureCheck[];
  boundaryYear: number;
  populations: PopulationForChart[];
}

interface TopProps {}

export class Top extends React.Component<TopProps, TopState> {
  constructor(props: TopProps, state: TopState) {
    super(props);
    this.state = {
      prefectures: [],
      boundaryYear: 0,
      populations: []
    };
    this.getPrefectures();
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(prefecCd: number) {
    // チェックされたボタンのcheckedとdisabledをtrueにして、
    // それ以外の全てdisabled
    const prefectures = this.state.prefectures;
    const tag = prefectures.filter(t => t.prefCode === prefecCd)[0];
    tag.checked = !tag.checked;
    tag.disabled = false;
    prefectures
      .filter(t => t.prefCode !== prefecCd)
      .forEach(t => (t.disabled = tag.checked ? true : false));
    this.setState({ prefectures: prefectures });
    if (tag.checked) {
      this.getPopulation(prefecCd);
    }
  }

  getPrefectures() {
    RestClient.getPrefectures().then(res => {
      const converted = res.result.map(t => {
        const after: PrefectureCheck = {
          checked: false,
          disabled: false,
          prefCode: t.prefCode,
          prefName: t.prefName
        };
        return after;
      });
      this.setState({
        prefectures: converted
      });
    });
  }

  getPopulation(prefCode: number) {
    RestClient.getPopulation(prefCode).then(res => {
      const converted = Converter.popConvert(res.result);

      this.setState({
        populations: converted,
        boundaryYear: res.result.boundaryYear
      });
    });
  }

  render() {
    return (
      <Container fixed>
        <Grid>
          <Grid item>
            <Typography variant="h2" component="h3" gutterBottom>
              都道府県別人口推移
            </Typography>
          </Grid>
          <Grid item>
            <PrefectureCheckBoxes
              changeCallback={this.handleChange}
              prefectures={this.state.prefectures}
            ></PrefectureCheckBoxes>
          </Grid>
          <Grid item>
            <Chart populations={this.state.populations}></Chart>
          </Grid>
        </Grid>
      </Container>
    );
  }
}
