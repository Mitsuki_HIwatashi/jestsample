import Fetcher from "../util/Fetcher";
import { PrefecturesRes } from "./data/PrefecturesRes";
import { PopulationsRes } from "./data/PopulationsRes";

async function getPrefectures() {
  const endpoint = process.env.REACT_APP_BACKEND_ENDPOINT as string;
  const url = "/api/v1/prefectures";
  return await Fetcher.fetcher<PrefecturesRes>(endpoint + url, {
    method: "GET",
    headers: {
      "X-API-KEY": process.env.REACT_APP_PERSONAL_ACCESS_TOKEN as string,
      "Content-Type": "application/json;charset=UTF-8"
    }
  });
}

async function getPopulation(prefCode: number) {
  const endpoint = process.env.REACT_APP_BACKEND_ENDPOINT as string;
  const url = "/api/v1/population/composition/perYear?";
  let params = new URLSearchParams();
  params.set("prefCode", prefCode.toString());
  params.set("cityCode", "-"); //全て
  return await Fetcher.fetcher<PopulationsRes>(
    endpoint + url + params.toString(),
    {
      headers: {
        "X-API-KEY": process.env.REACT_APP_PERSONAL_ACCESS_TOKEN as string,
        "Content-Type": "application/json;charset=UTF-8",
        Accept: "application/json"
      },
      method: "GET"
    }
  );
}

const RestClient = {
  getPrefectures: getPrefectures,
  getPopulation: getPopulation
};

export default RestClient;
