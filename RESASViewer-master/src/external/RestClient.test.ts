import Fetcher from "../util/Fetcher";
import RestClient from "./RestClient";
import { PrefecturesRes } from "./data/PrefecturesRes";
import { PopulationsRes } from "./data/PopulationsRes";

describe("RestClient", () => {
  test("getPrefectures", () => {
    const fetcherMock = jest
      .fn()
      // getRanks内のモック
      .mockImplementationOnce(() => {
        return {
          message: "message",
          result: [
            {
              prefCode: 1,
              prefName: "北海道"
            },
            {
              prefCode: 2,
              prefName: "青森県"
            }
          ]
        };
      });
    Fetcher.fetcher = fetcherMock;

    const res: Promise<PrefecturesRes> = RestClient.getPrefectures();
    res.then(elm => {
      expect(elm.message).toBe("message");
      expect(elm.result[1].prefCode).toBe(2);
    });
  });

  test("getPopulation", () => {
    const fetcherMock = jest
      .fn()
      // getRanks内のモック
      .mockImplementationOnce(() => {
        return {
          message: "message",
          result: {
            boundaryYear: 2015,
            data: [
              {
                label: "総人口",
                data: [{ year: 2015, value: 500 }]
              }
            ]
          }
        };
      });
    Fetcher.fetcher = fetcherMock;

    const res: Promise<PopulationsRes> = RestClient.getPopulation(1);
    res.then(elm => {
      expect(elm.message).toBe("message");
      expect(elm.result.boundaryYear).toBe(2015);
      expect(elm.result.data[0].label).toBe("総人口");
    });
  });
});
