export enum PopulationType {
  TOTAL = "総人口",
  YOUNG_AGE = "年少人口",
  OLD_AGE = "老年人口",
  WORKING_AGE = "生産年齢人口"
}
