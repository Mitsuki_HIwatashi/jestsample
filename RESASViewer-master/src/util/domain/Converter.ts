import { PopulationsByYear } from "../../external/data/PopulationsByYear";
import { PopulationForChart } from "../../components/data/PopulationForChart";
import { PopulationType } from "../../const/PopulationType";

function popConvert(source: PopulationsByYear) {
  const yearList = getYearList(source);
  const converted = yearList.map(year => {
    const totalVal = source.data
      .filter(t => t.label === PopulationType.TOTAL)[0]
      .data.filter(t => t.year === year)[0].value;
    const youngAgeVal = source.data
      .filter(t => t.label === PopulationType.YOUNG_AGE)[0]
      .data.filter(t => t.year === year)[0].value;
    const oldAgeVal = source.data
      .filter(t => t.label === PopulationType.OLD_AGE)[0]
      .data.filter(t => t.year === year)[0].value;
    const workinggeVal = source.data
      .filter(t => t.label === PopulationType.WORKING_AGE)[0]
      .data.filter(t => t.year === year)[0].value;
    const result: PopulationForChart = {
      year: year,
      total: totalVal,
      youngAge: youngAgeVal,
      oldAge: oldAgeVal,
      workingAge: workinggeVal
    };
    return result;
  });

  return converted;
}

function getYearList(source: PopulationsByYear) {
  return source.data[0].data.map(t => t.year);
}

const Converter = {
  popConvert: popConvert
};

export default Converter;
