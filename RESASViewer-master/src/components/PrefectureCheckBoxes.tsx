import React from "react";
import { FormControlLabel, Checkbox } from "@material-ui/core";
import { PrefectureCheck } from "./data/PrefectureCheck";

export interface PrefectureCheckBoxesProps {
  prefectures: PrefectureCheck[];
  changeCallback: Function;
}

export class PrefectureCheckBoxes extends React.Component<
  PrefectureCheckBoxesProps,
  {}
> {
  handlChange(prefCode: number) {
    this.props.changeCallback(prefCode);
  }
  render() {
    return (
      <div>
        {this.props.prefectures.map(t => {
          return (
            <FormControlLabel
              control={
                <Checkbox
                  checked={t.checked}
                  onChange={() => this.handlChange(t.prefCode)}
                  disabled={t.disabled}
                />
              }
              label={t.prefName}
            />
          );
        })}
      </div>
    );
  }
}
