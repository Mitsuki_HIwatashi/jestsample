import React from "react";
import renderer from "react-test-renderer";
import { mount } from "enzyme";
import { PrefectureCheckBoxes } from "./PrefectureCheckBoxes";

const testMock = jest.fn();

const prefectures = [
  {
    prefCode: 1,
    prefName: "北海道",
    checked: false,
    disabled: false
  },
  {
    prefCode: 2,
    prefName: "青森県",
    checked: false,
    disabled: false
  }
];

describe("Chart", () => {
  it("snapshot", () => {
    const tree = renderer
      .create(
        <PrefectureCheckBoxes
          prefectures={prefectures}
          changeCallback={testMock}
        />
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
  test("レンダリングの確認", () => {
    const wrapper = mount(
      <PrefectureCheckBoxes
        prefectures={prefectures}
        changeCallback={testMock}
      />
    );
    expect(wrapper.find("input").length).toBe(2);
  });

  test("propsを渡せてることの確認", () => {
    const wrapper = mount(
      <PrefectureCheckBoxes
        prefectures={prefectures}
        changeCallback={testMock}
      />
    );
    expect(wrapper.prop("prefectures").length).toBe(2);
    expect(wrapper.prop("changeCallback")).toBe(testMock);
  });

  test("コールバックの確認", () => {
    const wrapper = mount(
      <PrefectureCheckBoxes
        prefectures={prefectures}
        changeCallback={testMock}
      />
    );
    wrapper
      .find("input")
      .first()
      .simulate("change");
    expect(testMock).toHaveBeenCalled();
  });
});
