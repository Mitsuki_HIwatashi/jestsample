import { Prefecture } from "./Prefecture";

export interface PrefecturesRes {
  message: string;
  result: Prefecture[];
}
