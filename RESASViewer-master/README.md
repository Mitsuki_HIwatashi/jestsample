### RESASViewer


#### ■ Repository
[FrontEnd Project](https://github.com/UndyingSugimoto/RESASViewer)

#### ■ Overview
It is a Japanese population graph graph display service using RESAS-API.
[RESAS-API](https://opendata.resas-portal.go.jp/docs/api/v1/index.html)

■ Important point
Created in virtually one day.

■ Technology
- TypeScript
- React.js
- Material-UI
- Jest
