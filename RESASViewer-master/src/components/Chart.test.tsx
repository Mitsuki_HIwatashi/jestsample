import React from "react";
import renderer from "react-test-renderer";
import { Chart } from "./Chart";
import { mount } from "enzyme";

describe("Chart", () => {
  it("snapshot", () => {
    const tree = renderer.create(<Chart populations={[]} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
  test("レンダリングの確認", () => {
    const wrapper = mount(<Chart populations={[]} />);
    expect(wrapper.find(".recharts-wrapper").length).toBe(1);
  });
});
