import { Populations } from "./Populations";

export interface PopulationsByYear {
  boundaryYear: number;
  data: Populations[];
}
