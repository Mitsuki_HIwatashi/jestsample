import React from "react";
import {
  LineChart,
  CartesianGrid,
  XAxis,
  YAxis,
  Legend,
  Tooltip,
  Line
} from "recharts";
import { PopulationForChart } from "./data/PopulationForChart";

interface ChartProps {
  populations: PopulationForChart[];
}

export class Chart extends React.Component<ChartProps, {}> {
  render() {
    return (
      <LineChart
        width={1000}
        height={500}
        data={this.props.populations}
        margin={{
          top: 10,
          right: 10,
          left: 10,
          bottom: 10
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="year" />
        <YAxis width={100} />
        <Legend />
        <Tooltip />
        <Line type="monotone" dataKey="total" stroke="#8884d8" />
        <Line type="monotone" dataKey="youngAge" stroke="#82ca9d" />
        <Line type="monotone" dataKey="oldAge" stroke="#0000FF" />
        <Line type="monotone" dataKey="workingAge" stroke="#008080" />
      </LineChart>
    );
  }
}
