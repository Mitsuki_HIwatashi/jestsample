import { mount } from "enzyme";
import App from "./App";

import React from "react";
import { MemoryRouter } from "react-router-dom";
import { Top } from "./components/Top";

describe("App", () => {
  test("App", () => {
    const wrapper = mount(
      <MemoryRouter initialEntries={["/"]}>
        <App />
      </MemoryRouter>
    );
    expect(wrapper.find(Top)).toHaveLength(1);
  });
});
