import { PopulationsByYear } from "./PopulationsByYear";

export interface PopulationsRes {
  message: string;
  result: PopulationsByYear;
}
