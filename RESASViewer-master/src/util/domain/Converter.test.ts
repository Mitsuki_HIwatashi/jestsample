import Converter from "./Converter";
import { PopulationsByYear } from "../../external/data/PopulationsByYear";
import { PopulationType } from "../../const/PopulationType";

const source: PopulationsByYear = {
  boundaryYear: 2015,
  data: [
    {
      label: PopulationType.TOTAL,
      data: [
        {
          year: 1995,
          value: 500
        }
      ]
    },
    {
      label: PopulationType.YOUNG_AGE,
      data: [
        {
          year: 1995,
          value: 600
        }
      ]
    },
    {
      label: PopulationType.OLD_AGE,
      data: [
        {
          year: 1995,
          value: 700
        }
      ]
    },
    {
      label: PopulationType.WORKING_AGE,
      data: [
        {
          year: 1995,
          value: 800
        }
      ]
    }
  ]
};

describe("Converter", () => {
  test("popConvert", () => {
    const res = Converter.popConvert(source);
    expect(res[0].year).toBe(1995);
    expect(res[0].total).toBe(500);
    expect(res[0].youngAge).toBe(600);
    expect(res[0].oldAge).toBe(700);
    expect(res[0].workingAge).toBe(800);
  });
});
