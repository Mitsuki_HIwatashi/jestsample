export interface PopulationForChart {
  year: number;
  total: number;
  youngAge: number;
  workingAge: number;
  oldAge: number;
}
