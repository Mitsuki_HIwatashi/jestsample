import React from "react";
import renderer from "react-test-renderer";
import { Top } from "./Top";
import { mount } from "enzyme";
import RestClient from "../external/RestClient";
import { PrefecturesRes } from "../external/data/PrefecturesRes";
import { PopulationsRes } from "../external/data/PopulationsRes";

const getPopulationRes: PopulationsRes = {
  message: "message",
  result: {
    boundaryYear: 2015,
    data: [
      {
        label: "総人口",
        data: [{ year: 2015, value: 500 }]
      }
    ]
  }
};

const getPrefecturesRes: PrefecturesRes = {
  message: "message",
  result: [
    {
      prefCode: 1,
      prefName: "北海道"
    },
    {
      prefCode: 2,
      prefName: "青森県"
    }
  ]
};

describe("Top", () => {
  it("snapshot", async () => {
    const getPrefectures = jest
      .fn()
      .mockImplementation(() => Promise.resolve(getPrefecturesRes));
    RestClient.getPrefectures = getPrefectures;
    const tree = await renderer.create(<Top />);
    expect(tree.toJSON()).toMatchSnapshot();
  });
  test("レンダリングの確認", () => {
    const wrapper = mount(<Top />);
    expect(wrapper.find(".MuiTypography-gutterBottom").length).toBe(1);
  });

  test("各種ビジネスロジックの確認", async () => {
    // const getPrefectures = jest.spyOn(Top.prototype, "getPrefectures");
    // const getPopulation = jest.spyOn(Top.prototype, "getPopulation");

    const getPopulation = jest
      .fn()
      .mockImplementation(() => Promise.resolve(getPopulationRes));
    const getPrefectures = jest
      .fn()
      .mockImplementation(() => Promise.resolve(getPrefecturesRes));

    RestClient.getPopulation = getPopulation;
    RestClient.getPrefectures = getPrefectures;

    const wrapper = await mount<Top>(<Top />);
    expect(getPrefectures).toHaveBeenCalled();
    // 非同期関数をコールするので、マウントして再描画する
    // チェックボックスをチェック
    wrapper
      .mount()
      .find("input")
      .first()
      .simulate("change");
    expect(getPopulation).toHaveBeenCalled();

    expect(wrapper.state().prefectures[0].checked).toBe(true);
    expect(wrapper.state().prefectures[0].disabled).toBe(false);
    expect(wrapper.state().prefectures[1].disabled).toBe(true);

    // チェックボックスをチェック解除
    wrapper
      .find("input")
      .first()
      .simulate("change");

    expect(wrapper.state().prefectures[0].checked).toBe(false);
    expect(wrapper.state().prefectures[0].disabled).toBe(false);
    expect(wrapper.state().prefectures[1].disabled).toBe(false);
  });
});
